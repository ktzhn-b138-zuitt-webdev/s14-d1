// Single line comments (shortcut ctrl + /)


/*
	Multi-line comments
	- covers multiple line of comments
	- Shortcut ctrl + shift + /
*/

// Syntax and Statements
/*
	* Statements: instructions we tell the computer to perform
	* JS statements: usually end with a semicolon (;) 

	* Syntax: set of rules that describe how statements must be constructed
	* 

*/

console.log("Hello Batch 138!")

// Variables
/*
	- It is used to contain data.
	- Declaring variables - tells our devices that a variable name is created and is ready to store data 
	* SYNTAX:
		let/const variableName;
		- let keyword: used to handle values that will change over time
		- const [constant] keyword: used to handle values that are constant
	* VARIABLE NAMING:
		- camelCase
		- never start with a number
		- dont use special characters
		- avoid predefined JS keywords
*/

let myVariable;

console.log(myVariable);

// console.log(hello);

// Initializing values to variables

/*
	- Syntax: 
		let/const variableName = value;
*/

let productName = 'desktop computer' ;

console.log(productName);

let productPrice = 18999;

console.log(productPrice);

/* Reassigning variable values 

	- change the initial or previous value into another value

*/

productName = 'Laptop';

console.log(productName);

const interest = 3.539;

console.log(interest);

/*
interest = 4.489;

console.log(interest);
*/

let supplier; 

supplier = "John Smith Tradings";

console.log(supplier);

let quote = "He said 'I like pie' and I laughed";

// Declaring multiple variables

let productCode = "DC017";
const productBrand = "Dell";

console.log(productCode, productBrand);


// Data types

/* Strings
	- series of characters that create a word, phrase, or sentence, or anything related to creating text
	- strings in JS can be written with either with single ('') or double ("") quote. Use double anyway for best practice
*/

let country = 'Philippines';
let province = "Metro Manila"

// String concatentation
// Multiple string values can be combined into a single string using the "+" symbol;

// Output: Metro Manila, Philippines
let fullAddress = province + ", " + country;
console.log(fullAddress)

// Output: Welcome to Metro Manila, Philippines!


let welcome = "Welcome to " + fullAddress + "!";

console.log(welcome);

// Escape character
// \n : refers to creating a new line in betwen text

let mailAddress = "Mentro Manila\n\nPhilppines";

console.log(mailAddress)

let message = "John's employees went home early";
console.log(message)
message = 'John\'s employees went home early';
console.log(message)

// Numeric Data Types

// Integers/Whole Numbers
let headcount = 26;
console.log(headcount);

// Decimal Numbers
let grade = 98.7;
console.log(grade)

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining text and strings
// Output: John's grade last quarter is 98.7
console.log("John's grade last quarter is " + grade);

// Boolean
// 1/0 or True/False
let isMarried = true;
let isGoodConduct = false;

console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + isGoodConduct);

// Array: Special kind of data type used to store multiple values. Can store diff data types, but is normally used to store similar data types.

/* Syntax
	let/const arrayName = [element0, element1, elemen2, ...]
*/
let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

/* Objects 
	- another special kind of data type that is used to mimic real world objects/items
	SYNTAX:
		let/const objectName = {
			propertyA: value,
			property B: value
		}
*/

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["09196789010", "09992828822"],
	address: {
		houseNumber: 458,
		city: "Marikina"
	}
}

console.log(person);

// Null vs Undefined
/* Null
	- used to intentionally express the value in a variable declaration
*/

let spouse = null;
let money = 0;
let myName = "";

/* Undefined
	- represents the state of a variable that has been declared but without an assigned value

*/

let fullName;

/* Functions
	-  Functions in JS are lines/blocks of codes that tell our device/appplication to perform a certain task when called/invoked 

	Declaring function
	SYNTAX:
		function functionName(){
			line/block of codes;
		}
*/

// Declaring a function
function printName(){
	console.log("My name is John");
}

/*function printName(name){
	console.log(`My name is ${name}`);
}*/

// Invoking/calling a function
// printName();

// Declare a function that will display your favorite animal
function favAnimal(){
	console.log("My favorite animal: Frogs");
}

favAnimal();

// (name) - parameter
// Parameter: acts as a named variable/container that exists only inside of a function
function printName(name){
	console.log("My name is " + name);
}

// Argument: actual value that is provided a function for it to work properly 
printName("John");

printName("Jane");

function argumentFunction(){
	console.log("This function was passed as an argument before the message was printed");
}

function invokeFunction(argumentFunction){
	argumentFunction();
}

invokeFunction(argumentFunction);

// Finding more information about a function in the console
console.log(argumentFunction);

// Using multiple parameters

function createFullName(firstName, middleName, lastName){
	console.log("Hello " + firstName + " " + middleName + " " + lastName);
}

createFullName("Juan", "Pablo", "Cruz");
createFullName("Juan", "Pablo");
createFullName("Juan", "Pablo", "Cruz", "Junior");

// Parameters are different than variables
// Using variables as arguments
let firstName = "John";
let middleName = "Doe";
let firstName = "Smith";

createFullName(firstName, middleName, lastName);

// the "return" statement 
// - allows the output of a function  to be passed to the line/block of code that invoked/called the function
function returnFullName (firstName, middleName, lastName){
	return firstName + " " + middleName + " " + lastName;
	console.log("A simple message");
}

let completeName = returnFullName(firstName, middleName, lastName);
console.log(completeName);

console.log(returnFullName(firstName, middleName, lastName))

